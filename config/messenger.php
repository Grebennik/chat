<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Messenger Default User Model
    |--------------------------------------------------------------------------
    |
    | This option defines the default User model.
    |
    */

    'user' => [
        'model' => 'App\User'
    ],

    /*
    |--------------------------------------------------------------------------
    | Messenger Pusher Keys
    |--------------------------------------------------------------------------
    |
    | This option defines pusher keys.
    |
    */

    'pusher' => [
        'app_id'     => '752166',
        'app_key'    => 'af4f0ace5aa441089fd4',
        'app_secret' => '1c9fbb419bf9116ddae0',
        'options' => [
            'cluster'   => 'eu',
            'encrypted' => true
        ]
    ],
];
